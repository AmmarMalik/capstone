# PROJECT MEETING MINUTES

Project Title: Capstone project - Cohort 5

Project No: Cohort5-02

# Date: 05-24-2022
## Attendees: 
- Ammar S Malik
- Zahida Meeran
- swaroopa Rani nemani
- Geethanjali Ramamoorthy
- Sountharia Kumaran
- Chandra Lekha Ranganathan
- Shamel Arogundade
- Anju Gadodia

## Agenda:
- Last Meeting follow-up:

    - This is an initial kick-off meeting
- New business:

    - Kick-off this project

## Notes:
Kick-off this project

## Action Items:
- Define Project:

    Complete layout was defined including the database design and front end of the application. Also included was the scope of the project

- Define Team:

    A team was defined who will initially start working on this project. All cohort is welcomed to join if they what to

