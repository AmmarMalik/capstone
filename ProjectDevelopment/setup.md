# Project Details
### A list of all programs, utilities and packages used for this project

| Package | version | Reason | 
| ----------- | ----------- | ----------- |
| Python | 3.10.2 | Main coding language |
| Postgress | x.xx.xx | Database |
| Flask | package | Web server for Python |
|||