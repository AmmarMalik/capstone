# PROJECT MANAGEMENT PLAN

Project Title: Capstone project - Cohort 5

Project No: Cohort5-02

# Document History:
| Topic | Date | Description | 
| ----------- | ----------- | ----------- |
| Original PMP | 05-24-2022 | Initial project kick start |
|  | 05-24-2022 | Agenda defined and agreed |
|||

# Project Plan:
- Scope of the project:

    This project is designed to deploy a web based Student records database which will provision numerous aws microservices. These services will work together to define a secure and reliable website. This solution is more geared towards security, reliability and availabilty other that the actual front end to practice all norms and best practices.

- Microservices Used:

    Some of the microservies we are using are VPC, Auto Scaling, Load Balancing, S3, EC2, SNS, Lambda etc

# Project Delivery Team:
It has been defined who will be the team members of this project

- Ammar S Malik
- Zahida Meeran
- swaroopa Rani nemani
- Geethanjali Ramamoorthy
- Sountharia Kumaran
- Chandra Lekha Ranganathan
- Shamel Arogundade
- Anju Gadodia

# Constraints:
This project is based on the assumption that we know preliminary AWS microservices well. There will be a learning curve to process and apply this knowledge. We will be needing help from subject matter experts and study guides to complete the project 


# Work Breakdown Structure:

- Initial Plan timeline:

    1) Create git and Google drive 
    2) Create initial application environment that can access postgress database
    3) Deploy VPC environment
    4) Deploy Database in the private subnet
    5) Start using IAM, KMS and Parameter Store to secure the information
    6) Start using Lambda and SNS and see how to use SQS
    7) Deploy this application environment to Route 53
    8) Showcase the project

# Acquisition Plan:

# Resource Management:

Individual AWS accounts will be used to deploy this project. Route 53 might not be an option for some but the application will be up and running

# Schedule:

We need this project to be delivered by Wednesday June 01, 2022

# Project Quality Control:

# Risk Analysis:

# Safety & Occupational Health Plan:

# Change Management:

# Communication:

# Value Management:

# Closeout Plan:
- Working version of this application is deployed on all individual environments of all the team members