# capstone
Cohort-5 Capstone project

## Authors
- Ammar S Malik
- Zahida Meeran
- swaroopa Rani nemani
- Geethanjali Ramamoorthy
- Sountharia Kumaran
- Chandra Lekha Ranganathan
- Shamel Arogundade
- Anju Gadodia

## Project Details
This is a project to utilize as many micro services we can use. 
The front end does not matter. It is the services that means a lot. 
In order to get a complete details of the project, Please refer to the following 

## How to execute python web server locally

```
python --version
pip install virtualenv
python -m venv env
pip install -r requirements.txt
source env/Scripts/activate
python
from app import db
db.create_all()
exit()
Run app.py file
```
